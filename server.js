require('dotenv').config();
var express = require("express");
var async = require("async");
const Promise = require("promise");
const Path = require("path");
const md5 = require("md5");
const nodemailer = require('nodemailer');
const url = require("url");

var mysql = require("mysql");
var jade = require("jade");
var session = require("express-session");
var flash = require("express-flash");

var bodyParser = require("body-parser");
var multer = require("multer");
var upload = multer();
var sess;
console.log(process.env.NODEMAILER_HOST);



const app = express();
app.set('views',__dirname+'/views');
app.set('view engine','jade');

const {check,validationResult} = require("express-validator/check");
const {matchedData, sanitizeBody} = require('express-validator/filter');

// for parsing application/json
app.use(bodyParser.json()); 

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true })); 
//form-urlencoded

// for parsing multipart/form-data
app.use(upload.array()); 
app.use(express.static('public'));

app.use(session({secret:'jdfldsroiweuro309092830133fd3s',saveUninitialized: true,resave: true}));
app.use(flash());


var transporter = nodemailer.createTransport({
	/*service: 'gmail', */
	host: process.env.NODEMAILER_HOST,
	port: process.env.NODEMAILER_PORT,
	ssecure: process.env.NODEMAILER_SECURE,
	auth: {
		user: process.env.NODEMAILER_USERNAME,
		pass: process.env.NODEMAILER_PASSWORD
		}
	});




app.use('/*', function (req, res, next) {
  
	res.site = {};
	res.site.title = 'Project 1: ';
	
	set_env_variable(req,res);
	
	/*
	var q = url.parse(req.url, true);
  
	var allow_urls = [];
	var deny_urls = ["dashboard"];
	var after_login_redirect_urls = ["register","login","resetpassword"];
	var login_redirect_url = "dashboard";
	var uri = req.originalUrl;
	uri.slice(0,3);
	console.log(uri);
	console.log(uri.slice(0,1));
	console.log(req.path);
	//if(checkUserLoggedIn(req)){
		//if(after_login_redirect_urls.indexOf(value) > -1){
		//}
	//}
	*/
  
  
  
  next();
});

/*
 * MYSQL CONNECTION
 */

var con = mysql.createConnection({
	host: process.env.MYSQL_HOST,
	user: process.env.MYSQL_USERNAME,
	password: process.env.MYSQL_PASSWORD,
	database: process.env.MYSQL_DATABASE,
	debug: process.env.MYSQL_DEBUG,
	multipleStatements:process.env.MYSQL_DEBUG
});

/*
 * GLOBAL FUNCTIONS
 */

/*
 * CheckUser(email,password)
 * email - email of user that need to check on User table
 * password - password of user that need to check on User table
 * 
 * This is check whether active user is exists by email and password used before the login.
 * 
 */ 

function checkUser(email,password){
	return  new Promise(function(resolve,reject){
		var sql = "select count(id) as total_user FROM users WHERE email = ? and password = ? and status = 1";
		con.query(sql,[email,md5(password)],function(error,results,fields){
			if(error){
				throw error;
			}
			if(results[0].total_user==0){
				reject(new Error('E-mail/Password is wrong'))
			}else{
				resolve(true);
			}
		});
	});
}

function checkUserLoggedIn(req){
	var status = false;
	sess = req.session;
	if(sess.Front != undefined && sess.Front.A_User.id != undefined){
		status=true;
	}
	return status;
}

/*
 * checkUserEmail(value)
 * value - email of user that need to check on User table
 * 
 * This is check whether user email is exists on user table. This function used for user email validation.
 * 
 */
function checkUserEmail(value){
	return  new Promise(function(resolve,reject){
		var sql = "select count(*) as total_users FROM users WHERE email = ?";
		con.query(sql,[value],function(error,results,fields){
			if(error){
				throw error;
			}
			if(results[0].total_users >= 1){
				reject(new Error('E-mail already in use'))
			}else{
				resolve(true);
			}
		});
	});
}

function checkUserEmailExists(value){
	return  new Promise(function(resolve,reject){
		var sql = "select count(*) as total_users FROM users WHERE email = ?";
		con.query(sql,[value],function(error,results,fields){
			if(error){
				throw error;
			}
			console.log(results[0].total_users);
			if(results[0].total_users <= 0){
				reject(new Error('E-mail doesn\'t exists'))
			}else{
				resolve(true);
			}
		});
	});
}


function set_env_variable(req,res){
	var hostname = req.headers.host;
	var pathname = url.parse(req.url).pathname; // pathname = '/MyApp'
	process.env.SITE_BASEURL = req.protocol+'://' + hostname+'/';
}





app.get("/",function(req,res){
	//res.site.title = 'Updated one';
	sess=req.session;
	if(sess.Front != undefined && sess.Front.A_User.id != undefined){
		console.log(sess.Front);
		res.redirect('/dashboard');
	}else{
		res.render('home',{title:res.site.title});
	}
});




app.post("/login",[
	check("email")
		.trim()
		.isLength({ min: 1 }).withMessage('Email should not be blank')//This will not work with isEmail function
		.isEmail().withMessage('must be an email'),
	check("password")
		.trim()
		.isLength({ min: 1 }).withMessage('Password should not be blank')
		.custom((value, { req }) => {
			var is_user_found = false;
			return checkUser(req.body.email,value)
		})
	],function(req,res){
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			sess = req.session;
			sess.validationErrors = errors.mapped();
			res.redirect('/login');
		}else{
			res.site.error = '';
			async.waterfall([function (callback){
				sess=req.session;
				var error = '';
				var sql = "select * FROM users WHERE email = ? and password = ? and status = 1";
				con.query(sql,[req.body.email,md5(req.body.password)],function(error,results,fields){
					if(error){
						throw error;
					}
					if(results.length==0){
						req.flash('error', 'Email/Password is not correct');
						res.redirect('/login');
					}else{
						sess.Front = {
							A_User: results[0]
							};
						res.redirect('/dashboard');
					}
				});
			}],function(error,success){
				//console.log('success');
				//console.log(res.mydata);
				res.render('home',{title:res.site.title});
			});
		}
		
		
		
		
	});

app.get("/dashboard",function(req,res){
	if(checkUserLoggedIn(req)){
		res.render('dashboard',{title:res.site.title});
	}else{
		req.flash('error', 'Email/Password is not correct');
		res.redirect("/login");
	}

});

app.get("/login",function(req,res){
	res.site.title = 'Login';
	sess=req.session;
	var validationErrors = null;
	
	if(sess.validationErrors != undefined){
		validationErrors = sess.validationErrors; 
	}
	sess.validationErrors = null;
	
	if(checkUserLoggedIn(req)){
		res.redirect('/dashboard');
	}else{
		res.render('login',{title:res.site.title,validationErrors:validationErrors});
	}

});

app.get("/register",function(req,res){
	res.site.title = 'Register';
	sess=req.session;
	var validationErrors = null;
	
	if(sess.validationErrors != undefined){
		validationErrors = sess.validationErrors; 
	}
	sess.validationErrors = null;
	if(checkUserLoggedIn(req)){
		res.redirect('/dashboard');
	}else{
		res.render('register',{title:res.site.title,validationErrors:validationErrors});
	}

});




app.post("/register",[
	check("email")
		.trim()
		.isLength({ min: 1 }).withMessage('Email should not be blank')//This will not work with isEmail function
		.isEmail().withMessage('must be an email')
		.custom((value, { req }) => {
			var is_user_found = false;
			return checkUserEmail(value)
		}),
	check("name")
		.trim()
		.isLength({ min: 1 }).withMessage('Name should not be blank')
		.isLength({min:3,max:50}).withMessage('Name should be between 3 to 50 characters long'),
	check("password")
		.trim()
		.isLength({ min: 1 }).withMessage('Password should not be blank')
		.isLength({min:6,max:20}).withMessage('Name should be between 3 to 20 characters long'),
	check("confirmpassword","Confirm Password should be match with password.")
		.exists()
		.custom((value,{req}) => {
			return value===req.body.password;
			}),
	check("profilephoto","Profile photo should be jpg, gif, jpeg and png format.")
		.exists()
		.custom((value,{req}) => {
			var ext = (Path.extname(req.body.profilephoto)).toLowerCase();
			switch (ext) {
				case '.jpg':
				case '.jpeg':
				case  '.png':
				case  '.gif':
					return true;
					break;
				default:
					return false;
			}
			})
	],function(req,res){
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			sess = req.session;
			sess.validationErrors = errors.mapped();
			res.redirect('/register');
		}else{
			var sql = "INSERT INTO users SET name = ?, email = ? , password = ?, last_login = CURRENT_TIMESTAMP, created_at = NOW(), status = 1";
			con.query(sql,[req.body.name,req.body.email,md5(req.body.password)],function(error,results,fields){
				if(error){
					throw error;
				}
				var sql1 = "SELECT * FROM users WHERE id = ?";
				con.query(sql1,[results.insertId],function(error,results2,fields){
					if(error){
						throw error;
					}
					//console.log(results2);
					
					var mailOptions  = {
						from: 'himanshumahara@virtualemployee.com',
						to: results2[0].email,
						subject:'Successfully register to Project1',
						text:'Thankyou for registerwith us. You can login now.'
						};
						
					transporter.sendMail(mailOptions,function(error,info){
						
						if(error){
							console.log(error);
						}else{
							console.log(info.response);
						}
					});
					sess=req.session;
						sess.Front = {
								A_User: results2[0]
						};
						res.redirect('/dashboard');
					
					
				});
			});
		}

});

function findUserByEmail(value){
	return true;
}

app.get("/logout",function(req,res){
	console.log('on logout page');
	sess = req.session;
	sess.Front = null;
	res.redirect('/');
});


app.get("/resetpassword",function(req,res){
	res.site.title += ': Reset Password';
	sess=req.session;
	var validationErrors = null;
	
	if(sess.validationErrors != undefined){
		validationErrors = sess.validationErrors; 
	}
	sess.validationErrors = null;
	
	if(checkUserLoggedIn(req)){
		res.redirect('/dashboard');
	}else{
		res.render('resetpassword',{title:res.site.title,validationErrors:validationErrors});
	}

});


app.post("/resetpassword",[
	check("email")
		.trim()
		.isLength({ min: 1 }).withMessage('Email should not be blank')//This will not work with isEmail function
		.isEmail().withMessage('must be an email')
		.custom((value, { req }) => {
			var is_user_found = false;
			return checkUserEmailExists(value)
		})
	],function(req,res){
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			sess = req.session;
			sess.validationErrors = errors.mapped();
			res.redirect('/resetpassword');
		}
		
		
		if(checkUserLoggedIn(req)){
			res.redirect('/dashboard');
		}else{
			var sql = "UPDATE users SET reset_uri = ? WHERE email = ?";
			var datetime = new Date();
			var reset_uri = md5(datetime);
			console.log(reset_uri);
			con.query(sql,[reset_uri,req.body.email],function(error,results,fields){
				if(error){
					throw error;
				}
				
				var sql1 = "SELECT * FROM users WHERE email = ?";
				con.query(sql1,[req.body.email],function(error,results2,fields){
				if(error){
					throw error;
				}
				//console.log(results2);
				
				var mailOptions  = {
					from: 'himanshumahara@virtualemployee.com',
					to: results2[0].email,
					subject:'Reset password On Project 1',
					text:'Forget password uri: '+ process.env.SITE_BASEURL+ 'setyourpassword/' + reset_uri
					};
					
				transporter.sendMail(mailOptions,function(error,info){
					
					if(error){
						console.log(error);
					}else{
						console.log(info.response);
					}
				});
				res.redirect('/login');
				
				});
			});
		}
});

app.get("/setyourpassword/:reset_uri",function(req,res){
	res.site.title += ': Set your password';
	sess=req.session;
	var validationErrors = null;
	console.log(req.params);
	if(sess.validationErrors != undefined){
		validationErrors = sess.validationErrors; 
	}
	sess.validationErrors = null;
	res.render('setyourpassword',{title:res.site.title,validationErrors:validationErrors,reset_uri:req.params.reset_uri});

});



 app.use(function(req, res) {
      res.status(400);
     res.render('404.jade', {title: '404: File Not Found'});
  });
  
  // Handle 500
  app.use(function(error, req, res, next) {
      res.status(500);
     res.render('500.jade', {title:'500: Internal Server Error', error: error});
  });


app.listen(3000,() => console.log('Example app listening on port 3000!'));
